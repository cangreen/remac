// Калькулятор: скруглене бордеров у последней непустой ячейки в мобильной версии
if ($(window).width() <= 768) {
    const elems = $('.calculator__block').not('.calculator__block_empty');
    $(elems.get(elems.length - 1)).css('border-radius', '0 0 4px 4px');
}

//Установка наблюдателей для вылетающих элементов

$('.promo__image').each(function () {
    setObserver($(this), $(this).parent().parent().parent().get(0));
});
if ($('.team__image').length !== 0)
    setObserver($('.team__image'), $('.team').get(0));

function setObserver(target, observedElement) {
    const callback = (entries, observer) => {
        if (entries[0].intersectionRatio > 0.01) {
            $(target).show();
        }
    };
    const observer = new IntersectionObserver(callback, {
        rootMargin: '0px',
        threshold: 0.01
    });
    observer.observe(observedElement);
}

// Плавающий хедер

$(window).scroll(function () {
    if ($(window).width() >= 992) {
        const menu = $('.navbar__bottom');
        let scroll = $(window).scrollTop();
        if (scroll >= 40) {
            menu.removeClass('navbar-collapse');
            menu.slideUp(300)
            $('.navbar__contacts-block_fixed-hidden').hide();
            $('.menu_navbar-fixed').show();
            $('.navbar__contacts-block').not('.navbar__contacts-block_footer').addClass('navbar__contacts-block_lg-fixed');
            $('.navbar__contacts').not('.navbar__contacts_footer').addClass('navbar__contacts_lg-fixed');

        } else {
            menu.show();
            $('.menu_navbar-fixed').hide();
            $('.navbar__contacts-block_fixed-hidden').show();
            $('.navbar__contacts-block').removeClass('navbar__contacts-block_lg-fixed');
            $('.navbar__contacts').removeClass('navbar__contacts_lg-fixed');
        }
    }
});

// Счетчик девайсов

const callback = (entries, observer) => {
    if (entries[0].intersectionRatio > 0.01) {
        let time = 2;
        $('.summary__value').each(function () {
            $('.summary__block').show();
            let i = 1;
            let num = $(this).text();
            let step = 1000 * time / num;
            let that = $(this);
            let int = setInterval(function () {
                if (i <= num) {
                    that.html(i);
                } else {
                    clearInterval(int);
                }
                i++;
            }, step);
        });
    }
};

const summaryObserver = new IntersectionObserver(callback, {
    rootMargin: '0px',
    threshold: 0.01
});
if ($('.summary__row').length !== 0)
    summaryObserver.observe(document.querySelector('.summary__row'));

// Гуглокарта

function initMap() {
    var el = document.querySelector('.contacts__map');
    var styles = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#212121"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#707070"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#222222"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#fff"
                }
            ]
        },

        {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#707070"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#222222"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#181818"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#1b1b1b"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#2c2c2c"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#707070"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#222222"
                }
            ]
        },

        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#373737"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#3c3c3c"
                }
            ]
        },
        {
            "featureType": "road.highway.controlled_access",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#4e4e4e"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#000000"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#3d3d3d"
                }
            ]
        }
    ];
    var map = new google.maps.Map(el, {
        center: {lat: 60.009330, lng: 30.246200},
        zoom: 15,
        mapTypeControl: false,
        streetViewControl: false,
    });
    map.setOptions({styles: styles});
    var marker = new google.maps.Marker({
        position: {lat: 60.009330, lng: 30.246200},
        icon: {
            url: 'images/marker.svg',
        },
        map: map,
    });
}


// Плавный скроллинг

// Vanilla JavaScript Scroll to Anchor
// @ https://perishablepress.com/vanilla-javascript-scroll-anchor/

scrollTo();

function scrollTo() {
    var links = document.getElementsByTagName('a');
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        if ((link.href && link.href.indexOf('#') != -1) && ((link.pathname == location.pathname) || ('/' + link.pathname == location.pathname)) && (link.search == location.search)) {
            link.onclick = scrollAnchors;
        }
    }
}

function scrollAnchors(e, respond = null) {
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
    e.preventDefault();
    var targetID = (respond) ? respond.getAttribute('href') : this.getAttribute('href');
    let targetAnchor = null;
    if (targetID !== '#') {
        targetAnchor = document.querySelector(targetID);
    }

    if (!targetAnchor) return;
    const originalTop = distanceToTop(targetAnchor);
    const touchDeviceFix = $(window).width() <= 992 ? 50 : 0;
    window.scrollBy({top: originalTop - touchDeviceFix, left: 0, behavior: 'smooth'});
    const checkIfDone = setInterval(function () {
        const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
        if (distanceToTop(targetAnchor) === 0 || atBottom) {
            targetAnchor.tabIndex = '-1';
            targetAnchor.focus();
            window.history.pushState('', '', targetID);
            clearInterval(checkIfDone);
        }
    }, 100);
}


//Модаль
$('.modal__form').submit(function (e) {
    e.preventDefault();
    $('.contact__form').modal('hide');
    setTimeout(() => {
        $('.modal__success').modal('show')
    }, 400);

});

$('.contact__form').on('show.bs.modal', function () {

    if ($('.contact__form.show').length !== 0) {
        $('.contact__form.show').modal('hide');
    }
    if ($(window).width() <= 992) {
        $('.navbar').hide();
    }


});

$('.modal__success').on('hide.bs.modal', function () {
    $('.navbar').show();
});

$('.modal__success').on('show.bs.modal', function () {
    if ($(window).width() <= 992) {
        $('.navbar').hide();
    }
});

$('.contact__form').on('hide.bs.modal', function () {
    $('.navbar').show();
});


//Калькулятор

let scrollFix;
if ($(window).width() <= 768) {
    scrollFix = 50;
} else if ($(window).width() <= 992 && $(window).width() >= 768) {
    scrollFix = 80
} else {
    scrollFix = 0
}
$('.calculator__block_model').click(function (e) {
    e.preventDefault();
    $('.calculator').hide();
    $('.calculator__problem').show();
    $('html, body').animate({
        scrollTop: $('.calculator__problem').offset().top - scrollFix
    }, 400);
});

$('.calculator__block_problem').click(function (e) {
    const price = $(this).attr('data-price');
    let el = null;
    e.preventDefault();
    $('.calculator__problem').hide();
    if (!price) {
        el = $('.calculator__not-found');
        el.show();
    } else {
        el = $('.calculator_result');
        $('.advantages__block_sum strong').html(price + ' руб');
        $('.calculator_result .text-pink').html(price + ' руб');
        el.show();
    }

    $('html, body').animate({
        scrollTop: el.offset().top - scrollFix
    }, 400);
});

//Ленивая загрузка фоновых изображений

document.addEventListener('lazybeforeunveil', function (e) {
    var bg = e.target.getAttribute('data-bg');
    if (bg) {
        e.target.style.backgroundImage = 'url(' + bg + ')';
    }
});

//Ленивая загрузка скриптов

document.addEventListener('lazybeforeunveil', function (e) {
    var src = e.target.getAttribute('data-script');
    if (src) {
        var script = document.createElement('script');
        script.async = true;
        script.src = src;
        document.head.appendChild(script);
    }
});

// Маски полей с телефонами

$('.input__phone').mask("+7 (999) 999-99-99");

let windowWidth = $(window).width();

$(window).on('resize', function () {
    if ($(window).width() !== windowWidth) {
        windowWidth = $(window).width();
        $('.collapse').collapse('hide');

        if ($(window).width() <= 992) {
            $('.menu_navbar-fixed').hide();
        } else if ($(window).scrollTop() !== 0) {
            $('.menu_navbar-fixed').show()
        }
    }
});

//Скрытие бутерброда при открытии модали
if ($(window).width() <= 992) {
    $('#contactForm').on('shown.bs.modal', function () {
        $('.navbar__bottom').collapse('hide');
    });
}

//Скрытие бутерброда при клике на пункт меню
if ($(window).width() <= 992) {
    $('.menu__link').click(function () {
        $('.navbar__bottom').collapse('hide');
    });
}


